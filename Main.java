package pkg35r;

import java.awt.Graphics;
import java.awt.Color;
import javax.swing.JFrame;
import java.awt.Image;
import java.util.Random;

public class Main extends JFrame{
    final int WINDOW_WIDTH = 275, WINDOW_HEIGHT = 275;
    int xp2, xp, x, y, xTemp, yTemp, xRand, yRand, randVertex;
    int[] xVertices = {10,10,250}, yVertices = {30,250,250};
    Image screenState;
    Random rand = new Random();
    
    public Main(){
        super("Sierpinski's Triangle");
        xp = rand.nextInt(255) + 1;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(WINDOW_WIDTH,WINDOW_HEIGHT);
        setResizable(false);
        setVisible(true);
    }
    
    public void randXAndY(Graphics g){
        xRand = rand.nextInt(240);
        yRand = (250-xRand)*rand.nextInt()+xRand;
        x=xRand;
        y=yRand;
    }
    
    public void midPoint(Graphics g){
        randVertex = rand.nextInt(3) + 1;
        if(randVertex == 1){
            xTemp=(x + xVertices[0])/2;
            yTemp=(y + yVertices[0])/2;
            x=xTemp;
            y=yTemp;
            g.drawLine(x,y,x,y);
        }
        if(randVertex == 2){
            xTemp=(x + xVertices[1])/2;
            yTemp=(y + yVertices[1])/2;
            x=xTemp;
            y=yTemp;
            g.drawLine(x,y,x,y);
        }
        if(randVertex == 3){
            xTemp=(x + xVertices[2])/2;
            yTemp=(y + yVertices[2])/2;
            x=xTemp;
            y=yTemp;
            g.drawLine(x,y,x,y);
        }
    }
    
    @Override
    public void paint(Graphics g){
        g.setColor(Color.WHITE);
        g.fillRect(0,0,1000,1000);
        g.setColor(Color.BLACK);
        g.drawImage(screenState, 0, 0, this);
        g.drawPolygon(xVertices, yVertices, 3);
        randXAndY(g);
        for(int i = 0; i <= 100000; i++){
            midPoint(g);
        }
        g.setColor(Color.BLUE);
        g.drawString("Sierpinski!",150,100);
   }
    
    public static void main(String[] args) {
        new Main();
    }
}

